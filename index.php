<!DOCTYPE html>
<html>
	<head>

		<title>Projeto Hackathon</title>

		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />
		<link rel="icon" href="img/favicon.ico">
		
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
		<link rel="stylesheet" href="js/plugins/MarkerCluster.css">
		<link rel="stylesheet" href="js/plugins/MarkerCluster.Default.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
		<link rel="stylesheet" href="js/plugins/L.Control.Sidebar.css">
		<link rel="stylesheet" href="js/plugins/easy-button.css">
		
		<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
		<script src="js/plugins/leaflet-heat.js"></script>	
		<script src="js/plugins/leaflet.ajax.min.js"></script>
		<script src="js/plugins/leaflet.markercluster.js"></script>
		<script src="js/plugins/leaflet.markercluster-src.js"></script>
		<script src="js/plugins/leaflet.SliderControl.min.js"></script>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="js/plugins/L.Control.Sidebar.js"></script>
		<script src="js/plugins/easy-button.js"></script>
	 
		<style>
			
			#mapdiv {
				height: 100vh;
			}
			
			
			html, body {
				height: 100vh;
			}


			.info {
				padding: 6px 8px;
				font: 14px/16px Arial, Helvetica, sans-serif;
				background: white;
				background: rgba(255,255,255,0.8);
				box-shadow: 0 0 15px rgba(0,0,0,0.2);
				border-radius: 5px;
			}
			.info h4 {
				margin: 0 0 5px;
				color: #777;
			}

			.legend {
				line-height: 18px;
				color: #555;
			}
			.legend i {
				width: 18px;
				height: 18px;
				float: left;
				margin-right: 8px;
				opacity: 0.7;
			}
			
			.custom-select {
				font-size: 18px;
				position: relative;
				font-family: Arial;
				padding: 10px 30px;
			}

		</style>

	</head>

	<body>

		<div id="side-bar" class="col-md-3">
		
			<img src="img/fluxo.png" class="img-fluid" alt="">
		
			<div id="divProject" class="col-xs-12">
				<div id="divProjectLabel" class="text-center col-xs-12">
					<h4 id="lblProject">Análises Econômicas</h4>
				</div>
				
				<div class="custom-select" style="width:200px;">
				  <select id="mycombo">
					<option value="0">--- Selectione o tema ---</option>
					<option value="1">Beneficiários Programas Sociais</option>
					<option value="2">Inadimplência</option>
					<option value="3">Empresas Porte MEI</option>
				  </select>
				</div>

				<div class="slidecontainer">
					<input type="range" min="1" max="3" value="1" class="slider" id="myRange">
					<p>Ref: <span id="demo"></span></p>
				</div>
				
			</div>
			
			<img src="img/logo.png" class="img-fluid" width="30%" align="right" alt="Serasa">

		</div>


		<div id="mapdiv" class="col-md-12"></div> 
		
		
		<script>
			var ctlSidebar;
			var ctlEasybutton;
			var mymap;
			var lyrOSM;
			var lyrUf;
			var lyrBenef;
			var lyrHeatmap;
			var lyrCluster;
			var coords = [];
			var lyrClusterGrupo = new L.MarkerClusterGroup;
			var lyrTematicoSetores;
			var lyrTematicoMun;
			var lyrTematicoUf;
			var strFlds;
			
			$(document).ready(function() {
				
				mymap = L.map('mapdiv', {
					center:[-23.5759, -46.6584],
					zoom: 13
				});
				
				
				ctlSidebar = L.control.sidebar('side-bar').addTo(mymap);
			
				ctlEasybutton = L.easyButton('<img src="img/lupa.png">', function() {
					ctlSidebar.toggle();
				}).addTo(mymap);
		
		
				
				lyrOSM = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
				{
					maxZoom: 18,
					attribution: 'Map data &copy; <ahref="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
						'<ahref="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
						'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
					id: 'mapbox/streets-v11',
					tileSize: 512,
					zoomOffset: -1
				}).addTo(mymap);

				
				

				
		
		
				function load_heatMap(tema, periodo) {
					
					if (lyrClusterGrupo){
						mymap.removeLayer(lyrClusterGrupo);
					};	
					
					if (lyrTematicoSetores){
						mymap.removeLayer(lyrTematicoSetores);
					};	
					
					if (lyrTematicoMun){
						mymap.removeLayer(lyrTematicoMun);
					};	

					if (lyrTematicoUf){
						mymap.removeLayer(lyrTematicoUf);
					};	
					
					switch(tema){
						case '1':
							strURL = "content/benef_" + periodo + ".geojson";
							default:
					};
						
					$.ajax({

						url:strURL,
						success: function(response){
							
							coords = [];
							jsnHeatmap = JSON.parse(response);

							heatBenef = L.geoJSON(jsnHeatmap, {onEachFeature: function (feature, layer) {
								coords.push([feature.geometry.coordinates[1],feature.geometry.coordinates[0]]);
								layer.bindPopup('<h4>Coords: ' + feature.geometry.coordinates[1] + ',' + feature.geometry.coordinates[0] + '</h4>');
							}});
				
							if (lyrHeatmap){
								mymap.removeLayer(lyrHeatmap);
							};
							

							lyrHeatmap = L.heatLayer(coords,{
								radius: 30,
								blur: 15, 
								max: 3,
								gradient:{0.4: 'blue', 0.65: 'lime', 1: 'red'},
								maxZoom: 17
							}).addTo(mymap);
							
						},
						error: function(xhr, status, error) {
							alert("ERROR: "+error);
						}
					}); 
				};
				
				
				function load_cluster(tema, periodo) {
					
					if (lyrHeatmap){
						mymap.removeLayer(lyrHeatmap);
					};				
					
					if (lyrTematicoSetores){
						mymap.removeLayer(lyrTematicoSetores);
					};	
					
					if (lyrTematicoMun){
						mymap.removeLayer(lyrTematicoMun);
					};	
					
					if (lyrTematicoUf){
						mymap.removeLayer(lyrTematicoUf);
					};	
					
					switch(tema){
						case '3':
							strURL = "content/empresas_" + periodo + ".geojson";
							default:
					};
					
					
					$.ajax({url:strURL,
						success: function(response){
							jsnCluster = JSON.parse(response);
							lyrCluster = L.geoJSON(jsnCluster, {onEachFeature: function (feature, layer) {
								layer.bindPopup('<h4>Coords: ' + feature.geometry.coordinates[1] + ',' + feature.geometry.coordinates[0] + '</h4>');
							}});
							

							if (lyrClusterGrupo){
								mymap.removeLayer(lyrClusterGrupo);
							};
							
							lyrClusterGrupo.clearLayers();
							lyrClusterGrupo.addLayer(lyrCluster);
							lyrClusterGrupo.addTo(mymap);
						
						},
						error: function(xhr, status, error) {
							alert("ERROR: "+error);
						}
					}); 
				};
		


				mymap.on('moveend', function onDragEnd(){
										
					switch(combo.value) {
						case '2':
							nivel_tematico();
							break;
					};
				});


		
				function load_tematicosetores (tema, periodo) {
					
					if (lyrHeatmap){
						mymap.removeLayer(lyrHeatmap);
					};				
					
					if (lyrClusterGrupo){
						mymap.removeLayer(lyrClusterGrupo);
					};	
					
					if (lyrTematicoMun){
						mymap.removeLayer(lyrTematicoMun);
					};	
					
					if (lyrTematicoUf){
						mymap.removeLayer(lyrTematicoUf);
					};	
					
		
					$.ajax({
						url: "content/tematico_setores_Amostra.geojson",
						success: function (response) {
							jsnSetores = JSON.parse(response);
							
							if (lyrTematicoSetores){
								mymap.removeLayer(lyrTematicoSetores);
							};
							
							
							lyrTematicoSetores = L.geoJSON(jsnSetores, {style: setorStyle, onEachFeature: onEchFeatureSetor}).addTo(mymap);
						}
					});
				};
		

				function setorStyle(feature) {
					
					switch(slider.value) {
						case '1':
							var att = feature.properties.randonvalue1;
							break;
						case '2':
							var att = feature.properties.randonvalue2;
							break;
						case '3':
							var att = feature.properties.randonvalue3;
							break;
						default:
					};
					
					return {
						stroke: false,
						fillColor: getColor(att),
						weight: 1,
						opacity: 1,
						color: 'white',
						dashArray: '1',
						fillOpacity: 0.5
					};
				};

				
				function getColor(d) {
					return  d > 9000 ? '#2d0c75' :
							d > 8000 ? '#2d0c75' :
							d > 7000 ? '#3b0e9c' :
							d > 6000 ? '#3b0e9c' :
							d > 5000 ? '#BD43CD' :
							d > 3000 ? '#e699f0' :
							d > 1000 ? '#e699f0' :
									   '#e699f0' ;
				};
		
		
				function onEchFeatureSetor(feature, layer) {
					click: zoomToFeature
				};
		
				function zoomToFeature(e) {
					mymap.fitBounds(e.target.getBounds());
				};






				function load_tematicoMun (tema, periodo) {
					
					if (lyrHeatmap){
						mymap.removeLayer(lyrHeatmap);
					};				
					
					if (lyrClusterGrupo){
						mymap.removeLayer(lyrClusterGrupo);
					};	
					
					
					if (lyrTematicoSetores){
						mymap.removeLayer(lyrTematicoSetores);
					};	
					
					if (lyrTematicoUf){
						mymap.removeLayer(lyrTematicoUf);
					};	
									
					$.ajax({
						url: "content/tematico_municipios_Amostra.geojson",
						success: function (response) {
							jsnMunicipios = JSON.parse(response);
							
							if (lyrTematicoMun){
								mymap.removeLayer(lyrTematicoMun);
							};
							
							
							lyrTematicoMun = L.geoJSON(jsnMunicipios, {style: munStyle, onEachFeature: onEchFeatureSetor}).addTo(mymap);
						}
					});
				};
		
		
		
				function munStyle(feature) {
					
					switch(slider.value) {
						case '1':
							var att = feature.properties.randonvaluemun1;
							break;
						case '2':
							var att = feature.properties.randonvaluemun2;
							break;
						case '3':
							var att = feature.properties.randonvaluemun3;
							break;
						default:
					};
					
					return {
						stroke: false,
						fillColor: getColor(att),
						weight: 1,
						opacity: 1,
						color: 'white',
						dashArray: '1',
						fillOpacity: 0.5
					};
				};
		
		

		
				function load_tematicoUf (tema, periodo) {
					
					if (lyrHeatmap){
						mymap.removeLayer(lyrHeatmap);
					};				
					
					if (lyrClusterGrupo){
						mymap.removeLayer(lyrClusterGrupo);
					};	
					
					
					if (lyrTematicoSetores){
						mymap.removeLayer(lyrTematicoSetores);
					};	
					
					
					if (lyrTematicoMun){
						mymap.removeLayer(lyrTematicoMun);
					};	
					
					$.ajax({
						url: "content/tematico_uf_Amostra.geojson",
						success: function (response) {
							jsnUf = JSON.parse(response);
							
							if (lyrTematicoUf){
								mymap.removeLayer(lyrTematicoUf);
							};
							
							
							lyrTematicoUf = L.geoJSON(jsnUf, {style: ufStyle, onEachFeature: onEchFeatureSetor}).addTo(mymap);
						}
					});
				};
		
		
		
				function ufStyle(feature) {
					
					switch(slider.value) {
						case '1':
							var att = feature.properties.randonvalueuf1;
							break;
						case '2':
							var att = feature.properties.randonvalueuf2;
							break;
						case '3':
							var att = feature.properties.randonvalueuf3;
							break;
						default:
					};
					
					return {
						stroke: false,
						fillColor: getColor(att),
						weight: 1,
						opacity: 1,
						color: 'white',
						dashArray: '1',
						fillOpacity: 0.5
					};
				};

		
		
		
		
				function nivel_tematico() {
					var x = mymap.getZoom();
					switch(true) {
						case (x < 8):
							//uf
							load_tematicoUf(combo.value, slider.value);
							break;
						case (x < 12):
							//municipio
							load_tematicoMun(combo.value, slider.value);
							break;
						default:
							//setor
							load_tematicosetores(combo.value, slider.value);
							break;
					};
				};
		
		
				//*************** SLIDER PERIODO *************************************
				var slider = document.getElementById("myRange");
				var output = document.getElementById("demo");
		
				switch(slider.value) {
					case '1':
						output.innerHTML = 'Jan/20';
						break;
					case '2':
						output.innerHTML = 'Jun/20';
						break;
					case '3':
						output.innerHTML = 'Jan/21';
						break;
					default:
				};

				slider.oninput = function() {
					switch(this.value) {
						case '1':
							output.innerHTML = 'Jan/20';
							break;
						case '2':
							output.innerHTML = 'Jun/20';
							break;
						case '3':
							output.innerHTML = 'Jan/21';
							break;
						default:
					};
					
					switch(combo.value) {
						case '1':
							load_heatMap(combo.value, this.value);
							break;
						case '2':
							nivel_tematico();
							break;
						case '3':
							load_cluster(combo.value, this.value);
							break;
					};
				};
				
				
				
				//*************** COMBO TEMAS *************************************
				var combo = document.getElementById("mycombo");
				combo.oninput = function() {
					switch(this.value) {
						case '1':
							load_heatMap(this.value, slider.value);
							break;
						case '2':
							nivel_tematico();
							break;
						case '3':
							load_cluster(this.value, slider.value);
							break;
					};
				};
				

				
			});
		</script>
	</body>
</html>
